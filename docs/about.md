<h1> About </h1>

The material available on these pages is produced and maintained for a course on how to make your data analyses reproducible. In particular, it covers:

* Data management
* Conda
* Snakemake
* Git
* Jupyter
* R Markdown
* Docker

<h1> Material </h1>

Slides from lectures covering the topics above are available on Figshare as [pptx](https://ndownloader.figshare.com/files/13687424) or [pdf](https://ndownloader.figshare.com/files/13687421). If you're looking for slides/tutorials from a particular instance of the course, select the appropriate version tag in the popup panel at the bottom right. Go to [Schedule](schedule.md) for individual lectures.

This documentation and all the resources used in the course are available as a [Bitbucket repo](https://bitbucket.org/scilifelab-lts/reproducible_research_course.git).

A template directory and file structure consistent with the material described in the course is available as a [GitHub repo](https://github.com/NBISweden/project_template).

<h2> The authors </h2>
Leif Wigge, Rasmus Ågren and John Sundh
[SciLifeLab](https://www.scilifelab.se), [National Bioinformatics Infrastructure Sweden (NBIS)](https://www.nbis.se), Bioinformatics Long-term Support

<h2> License </h2>
MIT (see `LICENCE.txt` in the [Bitbucket repo](https://bitbucket.org/scilifelab-lts/reproducible_research_course.git)).
